package com.easyeat.actions

import com.easyeat.models.Ingredient
import com.easyeat.models.RecipeStep


class SetRecipeInfoAction(val title: String, val description: String)
class SetIngredientsAction(val ingredients: List<Ingredient>)
class AddStepAction(val step: RecipeStep)