package com.easyeat.actions

import com.easyeat.models.Recipe

class RecipeSelectedAction(val recipe: Recipe)
