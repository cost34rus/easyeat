package com.easyeat.models

import com.easyeat.extensions.toArrayList

class PlanDay(
    var name: String,
    var description: String,
    var sections: ArrayList<PlanDaySection> = arrayListOf()
) {
    private val defaultSections: List<String> = listOf("Завтрак", "Обед", "Ужин")

    init {
        sections = defaultSections.map { PlanDaySection(it) }.toArrayList()
    }
}

class PlanDaySection(
    var name: String = "",
    var recipes: ArrayList<Recipe> = arrayListOf()
)