package com.easyeat.models

import java.util.*

open class Recipe(
    var title: String,
    var description: String,
    var id: String = UUID.randomUUID().toString(),
    var ingredients: List<Ingredient> = arrayListOf(),
    var steps: ArrayList<RecipeStep> = arrayListOf()
)

open class RecipeStep(var description: String)

open class Ingredient(var name: String)