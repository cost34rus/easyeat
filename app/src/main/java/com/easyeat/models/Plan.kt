package com.easyeat.models

import com.easyeat.extensions.toArrayList
import java.util.*

class Plan(
    var id: String = UUID.randomUUID().toString(),
    var title: String = "",
    var description: String = "",
    var days: ArrayList<PlanDay> = arrayListOf()
) {
    private var daysOfWeek: Map<Int, String> = hashMapOf(
        0 to "Понедельник",
        1 to "Вторник",
        2 to "Среда",
        3 to "Четверг",
        4 to "Пятница",
        5 to "Суббота",
        6 to "Воскресение"
    )

    init {
        days = daysOfWeek.map { PlanDay(it.value, it.value) }.toArrayList()
    }
}
