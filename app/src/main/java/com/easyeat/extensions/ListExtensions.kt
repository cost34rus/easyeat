package com.easyeat.extensions

fun <T> List<T>.toArrayList(): ArrayList<T> =  ArrayList(this)