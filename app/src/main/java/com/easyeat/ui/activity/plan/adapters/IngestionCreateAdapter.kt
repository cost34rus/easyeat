package com.easyeat.ui.activity.plan.adapters

import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.RecyclerView
import com.easyeat.R
import com.easyeat.models.PlanDaySection
import com.easyeat.presentation.presenter.plan.PlanEditPresenter
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionParameters
import io.github.luizgrp.sectionedrecyclerviewadapter.StatelessSection

class IngestionCreateAdapter(
    private val mPlanEditPresenter: PlanEditPresenter,
    private val section: PlanDaySection
) : StatelessSection(
    SectionParameters.builder()
        .itemResourceId(R.layout.recipe_mini_card)
        .headerResourceId(R.layout.ingestion_create)
        .build()
) {

    override fun getContentItemsTotal(): Int {
        return section.recipes.size
    }

    override fun getItemViewHolder(view: View): RecyclerView.ViewHolder {
        return ItemViewHolder(view)
    }

    override fun onBindItemViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        val itemHolder = holder as ItemViewHolder

        val name = section.recipes[position]?.title
        val category = section.recipes[position]?.description

        with(itemHolder) {
            item.text = name
            subItem.text = category
        }
    }

    override fun getHeaderViewHolder(view: View): RecyclerView.ViewHolder {
        return HeaderViewHolder(view)
    }

    override fun onBindHeaderViewHolder(holder: RecyclerView.ViewHolder) {
        val headerHolder = holder as HeaderViewHolder
        headerHolder.title.title = section.name

        headerHolder.btnAdd.setOnClickListener {
            mPlanEditPresenter.startRecipeSelect(section)
        }
    }
}

class HeaderViewHolder(val rootView: View) : RecyclerView.ViewHolder(rootView) {
    val title: Toolbar = rootView.findViewById(R.id.ingestionToolBar)
    val btnAdd: Button = rootView.findViewById(R.id.ingestionBtnAdd)
}

class ItemViewHolder(rootView: View) : RecyclerView.ViewHolder(rootView) {
    val item: TextView = rootView.findViewById(R.id.tvItem)
    val subItem: TextView = rootView.findViewById(R.id.tvSubItem)
}