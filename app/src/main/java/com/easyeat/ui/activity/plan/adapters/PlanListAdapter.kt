package com.easyeat.ui.activity.plan.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.easyeat.R
import com.easyeat.models.Plan

class PlanListAdapter(private val planList: List<Plan>) : RecyclerView.Adapter<PlanListAdapter.ViewHolder>() {
    override fun getItemCount(): Int = planList.size

    override fun onBindViewHolder(holder: ViewHolder, i: Int) {
        val plan = planList[i]
        holder.planItemTitle.text = plan.title
        holder.planItemDescription.text = plan.description
        val daysAdapter = DayMiniCardAdapter(plan.days)
        holder.planItemDays.adapter = daysAdapter
        daysAdapter.notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.plan_list_item, parent, false)
        return ViewHolder(v)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var planItemTitle: TextView = itemView.findViewById(R.id.planItemTitle)
        var planItemDescription: TextView = itemView.findViewById(R.id.planItemDescription)
        var planItemDays: RecyclerView = itemView.findViewById(R.id.planItemDays)
    }
}