package com.easyeat.ui.activity.recipe.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.easyeat.R
import com.easyeat.models.Recipe
import com.easyeat.presentation.presenter.recipe.RecipeSelectPresenter

class RecipeListAdapter(
    private val recipeList: List<Recipe>,
    private val mRecipeSelectPresenter: RecipeSelectPresenter
) : RecyclerView.Adapter<RecipeListAdapter.ViewHolder>() {
    override fun getItemCount(): Int = recipeList.size

    override fun onBindViewHolder(holder: ViewHolder, i: Int) {
        val recipe = recipeList[i]
        holder.itemView.setOnClickListener {
            mRecipeSelectPresenter.selectRecipe(recipe)
        }

        holder.title.text = recipe.title
        holder.description.text = recipe.description
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.recipe_mini_card, parent, false)
        return ViewHolder(v)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var title: TextView = itemView.findViewById(R.id.tvItem)
        var description: TextView = itemView.findViewById(R.id.tvSubItem)
    }
}