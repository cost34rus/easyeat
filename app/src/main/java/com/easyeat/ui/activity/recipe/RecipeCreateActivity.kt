package com.easyeat.ui.activity.recipe

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.navigation.fragment.NavHostFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.easyeat.R
import com.easyeat.presentation.presenter.recipe.RecipeCreatePresenter
import com.easyeat.presentation.view.recipe.RecipeCreateView
import com.easyeat.ui.activity.base.MvpCompatActivity
import com.easyeat.ui.activity.base.ToolbarManager
import com.easyeat.ui.fragment.recipe.RecipeIngredientsFragment
import com.thekhaeng.pushdownanim.PushDownAnim
import kotlinx.android.synthetic.main.activity_recipe_create.*


class RecipeCreateActivity : MvpCompatActivity(), RecipeCreateView {

    companion object {
        const val TAG = "RecipeCreateActivity"
        fun getIntent(context: Context): Intent = Intent(context, RecipeCreateActivity::class.java)
    }


    @InjectPresenter
    lateinit var mRecipeEditPresenter: RecipeCreatePresenter

    override fun onSupportNavigateUp(): Boolean {
        return NavHostFragment.findNavController(nav_host_fragment).navigateUp()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recipe_create)
    }

    override fun initToolbar() {
        ToolbarManager().initForNavigation(this, "Новый рецепт")
    }

    override fun initActions() {
        val navController = NavHostFragment.findNavController(nav_host_fragment)
        var stepsNumber = 1

        fun navigate(res: Int) {
            val bundle = Bundle()
            bundle.putInt("step", stepsNumber)
            navController.navigate(res, bundle)
            stepsNumber++
        }
        PushDownAnim.setPushDownAnimTo(next).setScale(PushDownAnim.MODE_STATIC_DP, 5F).setOnClickListener {
            when (navController.currentDestination!!.label) {
                "RecipeInfoFragment" -> navController.navigate(R.id.action_recipeInfoFragment_to_recipeIngredientsFragment)
                "RecipeIngredientsFragment" -> navController.navigate(R.id.action_recipeIngredientsFragment_to_recipeStepFragment)
                "RecipeStepFragment" -> navigate(R.id.action_recipeStepFragment_self)
            }
        }
    }

    override fun recipeLoad() {
    }
}
