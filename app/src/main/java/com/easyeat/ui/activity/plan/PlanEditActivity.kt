package com.easyeat.ui.activity.plan

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.TypedValue
import android.view.Gravity
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import com.arellomobile.mvp.presenter.InjectPresenter
import com.easyeat.R
import com.easyeat.models.PlanDaySection
import com.easyeat.presentation.presenter.plan.PlanEditPresenter
import com.easyeat.presentation.view.plan.PlanEditView
import com.easyeat.ui.activity.base.SlidrActivityBase
import com.easyeat.ui.activity.plan.adapters.PlanDayPageAdapter
import com.easyeat.ui.activity.recipe.RecipeSelectActivity
import com.tmall.ultraviewpager.UltraViewPager
import kotlinx.android.synthetic.main.activity_add_plan.*

class PlanEditActivity : SlidrActivityBase(), PlanEditView {

    @InjectPresenter
    lateinit var mPlanEditPresenter: PlanEditPresenter

    companion object {
        const val TAG = "PlanEditActivity"
        fun getIntent(context: Context): Intent = Intent(context, PlanEditActivity::class.java)
    }

    override fun initToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        toolbar.title = "Новый план"
        toolbar.setNavigationOnClickListener {
            finish()
        }

        toolbar.findViewById<Button>(R.id.confirm).setOnClickListener {
            val titleInput = findViewById<TextView>(R.id.title)
            val descptionInput = findViewById<TextView>(R.id.description)
            mPlanEditPresenter.savePlan(titleInput.text.toString(), descptionInput.text.toString())
            finish()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_plan)

        with(ultra_viewpager) {
            viewPager.pageMargin = 8
            setScrollMode(UltraViewPager.ScrollMode.HORIZONTAL)
            setMultiScreen(0.9f)
            setItemMargin(0, 0, 8, 0)
            setAutoMeasureHeight(true)
            initIndicator()
            indicator
                .setMargin(0, 0, 0, 16)
                .setOrientation(UltraViewPager.Orientation.HORIZONTAL)
                .setFocusColor(Color.GREEN)
                .setNormalColor(Color.WHITE)
                .setRadius(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5f, resources.displayMetrics).toInt())
                .setGravity(Gravity.CENTER_HORIZONTAL or Gravity.BOTTOM)
                .build()
        }
    }

    override fun onPlanLoaded() {
        ultra_viewpager.adapter = PlanDayPageAdapter(this.mPlanEditPresenter)
    }

    override fun onStartRecipeSelect() {
        startActivity(RecipeSelectActivity.getIntent(this))
    }

    override fun onRecipeSelected(planDaySection: PlanDaySection) {
        (ultra_viewpager.adapter as PlanDayPageAdapter).updateSection(planDaySection)
    }
}
