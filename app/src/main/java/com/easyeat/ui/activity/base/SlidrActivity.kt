package com.easyeat.ui.activity.base

import android.os.Bundle
import com.r0adkll.slidr.Slidr
import com.r0adkll.slidr.model.SlidrConfig
import com.r0adkll.slidr.model.SlidrPosition

open class SlidrActivityBase : MvpCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initSlidr()
    }

    protected fun initSlidr() {
        val mConfig = SlidrConfig.Builder()
            .position(SlidrPosition.TOP)
            .edge(true)
            .touchSize(50f)
            .build()

        Slidr.attach(this, mConfig)
    }
}
