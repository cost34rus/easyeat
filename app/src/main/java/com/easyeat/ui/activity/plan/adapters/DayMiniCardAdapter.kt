package com.easyeat.ui.activity.plan.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.easyeat.R
import com.easyeat.models.PlanDay

class DayMiniCardAdapter(private val dayList: List<PlanDay>) : RecyclerView.Adapter<DayMiniCardAdapter.ViewHolder>() {
    override fun getItemCount(): Int = dayList.size

    override fun onBindViewHolder(holder: ViewHolder, i: Int) {
        val day = dayList[i]
        holder.dayTitle.text = day.name
        holder.dayDescription.text = day.description
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.day_mini_card, parent, false)
        return ViewHolder(v)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var dayTitle: TextView = itemView.findViewById(R.id.dayItemTitle)
        var dayDescription: TextView = itemView.findViewById(R.id.dayItemDescription)
    }
}