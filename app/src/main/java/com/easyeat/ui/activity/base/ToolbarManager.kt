package com.easyeat.ui.activity.base

import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.easyeat.R

data class ToolbarOptions(
    val title: String = "",
    val titleIsShow: Boolean = true,
    val confirmAction: (view: View) -> Unit = {},
    val confirmIsShow: Boolean = false,
    val navigationAction: (view: View) -> Unit = {},
    val navigationIsShow: Boolean = false
)

class ToolbarManager {

    fun initForNavigation(
        activity: AppCompatActivity,
        title: String = "",
        confirmAction: ((view: View) -> Unit)? = null
    ): Toolbar {
        val toolbar = activity.findViewById<Toolbar>(R.id.toolbar)
        activity.setSupportActionBar(toolbar)

        toolbar.title = title

        val confirm = toolbar.findViewById<Button>(R.id.confirm)
        if (confirmAction == null)
            confirm.visibility = View.GONE
        else
            confirm.setOnClickListener(confirmAction)

        toolbar.setNavigationOnClickListener {
            activity.finish()
        }
        return toolbar
    }
}