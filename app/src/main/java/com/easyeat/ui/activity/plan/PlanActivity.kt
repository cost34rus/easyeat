package com.easyeat.ui.activity.plan

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.View
import android.widget.Button
import androidx.appcompat.widget.Toolbar
import com.arellomobile.mvp.presenter.InjectPresenter
import com.easyeat.R
import com.easyeat.models.Plan
import com.easyeat.presentation.presenter.plan.PlanPresenter
import com.easyeat.presentation.view.plan.PlanView
import com.easyeat.ui.activity.base.MvpCompatActivity
import com.easyeat.ui.activity.plan.adapters.PlanListAdapter
import com.easyeat.ui.activity.recipe.RecipeCreateActivity
import kotlinx.android.synthetic.main.activity_plan.*


class PlanActivity : MvpCompatActivity(), PlanView {

    companion object {
        const val TAG = "PlanActivity"
        fun getIntent(context: Context): Intent = Intent(context, PlanActivity::class.java)
    }

    @InjectPresenter
    lateinit var mPlanPresenter: PlanPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_plan)
    }

    override fun onPlanLoad(plans: List<Plan>) {
        val adapter = PlanListAdapter(plans)
        planList.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    override fun initToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        toolbar.title = "EasyEat"
        toolbar.navigationIcon = null
        toolbar.findViewById<Button>(R.id.confirm).visibility = View.GONE
        setSupportActionBar(toolbar)
    }

    override fun initMenuActions() {
        menu_item_new_recipe.setOnClickListener {
            startActivity(RecipeCreateActivity.getIntent(this))
        }
        menu_item_new_plan.setOnClickListener {
            startActivity(PlanEditActivity.getIntent(this))
        }
    }


}
