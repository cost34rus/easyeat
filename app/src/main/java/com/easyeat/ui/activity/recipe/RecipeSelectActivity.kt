package com.easyeat.ui.activity.recipe

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import com.arellomobile.mvp.MvpActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.easyeat.R
import com.easyeat.models.PlanDay
import com.easyeat.models.Recipe
import com.easyeat.presentation.presenter.recipe.RecipeSelectPresenter
import com.easyeat.presentation.view.recipe.RecipeSelectView
import com.easyeat.ui.activity.base.MvpCompatActivity
import com.easyeat.ui.activity.base.ToolbarManager
import com.easyeat.ui.activity.recipe.adapters.RecipeListAdapter
import kotlinx.android.synthetic.main.activity_recipe_select.*


class RecipeSelectActivity : MvpCompatActivity(), RecipeSelectView {

    companion object {
        const val TAG = "RecipeSelectActivity"
        fun getIntent(context: Context): Intent = Intent(context, RecipeSelectActivity::class.java)
    }

    @InjectPresenter
    lateinit var mRecipeSelectPresenter: RecipeSelectPresenter

    override fun initToolbar() {
        ToolbarManager().initForNavigation(this, "Выбор рецепта")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recipe_select)
    }

    override fun onRecipeLoad(recipeList: List<Recipe>) {
        val adapter = RecipeListAdapter(recipeList, mRecipeSelectPresenter)
        recipeListView.adapter = adapter
        adapter.notifyItemRangeInserted(0, recipeList.size)
    }

    override fun onRecipeSelected(recipe: Recipe) {
        finish()
    }
}
