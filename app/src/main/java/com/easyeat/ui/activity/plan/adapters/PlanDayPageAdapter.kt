package com.easyeat.ui.activity.plan.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.PagerAdapter
import com.easyeat.R
import com.easyeat.models.PlanDay
import com.easyeat.models.PlanDaySection
import com.easyeat.presentation.presenter.plan.PlanEditPresenter
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter

class PlanDayPageAdapter(private val editPresenter: PlanEditPresenter) : PagerAdapter() {

    private val sectionsMap = mutableMapOf<PlanDaySection, SectionedRecyclerViewAdapter>()

    override fun getCount(): Int = 7

    override fun isViewFromObject(view: View, obj: Any): Boolean {
        return view === obj
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val linearLayout = LayoutInflater
            .from(container.context)
            .inflate(R.layout.day_create, null) as LinearLayout

        val planDay = editPresenter.getDayByNumber(position)
        linearLayout.id = R.id.item_id

        val dayPageTitle = linearLayout.findViewById<TextView>(R.id.dayPageTitle)
        dayPageTitle.text = planDay.name

        initSections(linearLayout, planDay)
        container.addView(linearLayout)
        return linearLayout
    }

    override fun destroyItem(container: ViewGroup, position: Int, obj: Any) {
        val view = obj as LinearLayout
        container.removeView(view)
    }

    private fun initSections(linearLayout: LinearLayout, planDay: PlanDay) {
        val sectionAdapter = SectionedRecyclerViewAdapter()
        val glm = GridLayoutManager(linearLayout.context, 2)
        glm.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return when (sectionAdapter.getSectionItemViewType(position)) {
                    SectionedRecyclerViewAdapter.VIEW_TYPE_HEADER -> 2
                    else -> 1
                }
            }
        }

        val dayRecipeGroupRecyclerView = linearLayout.findViewById<RecyclerView>(R.id.dayRecipeGroupRecyclerView)
        dayRecipeGroupRecyclerView.layoutManager = glm
        dayRecipeGroupRecyclerView.adapter = sectionAdapter

        for (s in planDay.sections) {
            addNewSectionToAdapter(s, dayRecipeGroupRecyclerView, sectionAdapter)
        }
    }

    private fun addNewSectionToAdapter(planDaySection: PlanDaySection, view: RecyclerView, sectionAdapter: SectionedRecyclerViewAdapter) {
        sectionsMap[planDaySection] = sectionAdapter
        val section = IngestionCreateAdapter(editPresenter, planDaySection)
        sectionAdapter.addSection(planDaySection.name, section)
        val sectionPos = sectionAdapter.getSectionPosition(planDaySection.name)
        sectionAdapter.notifyItemInserted(sectionPos)
        view.smoothScrollToPosition(sectionPos)
        sectionAdapter.notifyDataSetChanged()
    }

    fun updateSection(planDaySection: PlanDaySection) {
        val sectionAdapter = sectionsMap[planDaySection]
        sectionAdapter?.notifyAllItemsChangedInSection(planDaySection.name)
    }
}
