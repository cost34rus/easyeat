package com.easyeat.ui.fragment.recipe

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.easyeat.R
import com.easyeat.actions.SetIngredientsAction
import com.easyeat.models.Ingredient
import com.easyeat.presentation.view.recipe.RecipeIngridientsView
import com.easyeat.ui.fragment.recipe.adapters.IngredientItemAdapter
import com.easyeat.ui.fragment.recipe.helpers.SwipeToDeleteCallback
import kotlinx.android.synthetic.main.fragment_recipe_ingridients.*
import org.greenrobot.eventbus.EventBus

class RecipeIngredientsFragment : Fragment(), RecipeIngridientsView {

    private val ingredients = mutableListOf<String>()
    private val simpleAdapter = IngredientItemAdapter(ingredients)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_recipe_ingridients, container, false)
    }

    override fun onStart() {
        super.onStart()

        btnAdd.setOnClickListener {
            val text = textInput.text.toString()
            simpleAdapter.addItem(text)
            textInput.text = null
        }

        recyclerView.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = simpleAdapter

        val swipeHandler = object : SwipeToDeleteCallback(context!!) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val adapter = recyclerView.adapter as IngredientItemAdapter
                adapter.removeAt(viewHolder.adapterPosition)
            }
        }
        val itemTouchHelper = ItemTouchHelper(swipeHandler)
        itemTouchHelper.attachToRecyclerView(recyclerView)
    }

    override fun onPause() {
        super.onPause()
        EventBus.getDefault().post(SetIngredientsAction(ingredients.map { Ingredient(it) }))
    }
}
