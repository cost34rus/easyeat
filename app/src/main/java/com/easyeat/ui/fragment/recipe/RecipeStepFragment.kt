package com.easyeat.ui.fragment.recipe

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.easyeat.R
import com.easyeat.actions.AddStepAction
import com.easyeat.models.RecipeStep
import kotlinx.android.synthetic.main.fragment_recipe_step.*
import org.greenrobot.eventbus.EventBus

class RecipeStepFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_recipe_step, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val step = arguments!!.getInt("step")
        stepNumber.text = step.toString()
    }

    override fun onPause() {
        super.onPause()
        EventBus.getDefault().post(AddStepAction(RecipeStep("")))
    }
}
