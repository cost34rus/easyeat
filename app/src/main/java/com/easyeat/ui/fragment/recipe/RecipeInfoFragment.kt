package com.easyeat.ui.fragment.recipe

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.easyeat.R
import com.easyeat.actions.SetRecipeInfoAction
import kotlinx.android.synthetic.main.fragment_recipe_info.*
import org.greenrobot.eventbus.EventBus

class RecipeInfoFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_recipe_info, container, false)
    }

    override fun onPause() {
        super.onPause()
        val title = title.text.toString()
        val description = description.text.toString()
        EventBus.getDefault().post(SetRecipeInfoAction(title, description))
    }
}
