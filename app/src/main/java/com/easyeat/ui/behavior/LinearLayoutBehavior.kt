package com.easyeat.ui.behavior

import android.view.View
import android.widget.LinearLayout
import androidx.appcompat.widget.Toolbar
import androidx.coordinatorlayout.widget.CoordinatorLayout
import com.easyeat.R

class LinearLayoutBehavior : CoordinatorLayout.Behavior<LinearLayout>() {
    override fun onDependentViewChanged(parent: CoordinatorLayout, child: LinearLayout, dependency: View): Boolean {
        val toolbar = parent.findViewById<Toolbar>(R.id.toolbar)
        dependency.setPadding(0, toolbar.height, 0, 0)
        return true
    }
}