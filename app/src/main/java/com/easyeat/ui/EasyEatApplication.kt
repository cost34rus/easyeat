package com.easyeat.ui

import android.app.Application
import com.easyeat.models.Books
import com.easyeat.models.Recipe
import io.paperdb.Paper

class EasyEatApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        Paper.init(this)
        addDefaultRecipe()
    }

    private fun addDefaultRecipe() {
        val recipes = Paper.book(Books.Recipe)
        if (recipes.allKeys.size > 0)
            return

        val recipe = Recipe("1", "1")
        recipes.write(recipe.id, recipe)
    }
}