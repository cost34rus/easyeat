package com.easyeat.presentation.presenter.plan

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.easyeat.actions.PlanSaveAction
import com.easyeat.actions.RecipeSelectedAction
import com.easyeat.models.Books
import com.easyeat.models.Plan
import com.easyeat.models.PlanDay
import com.easyeat.models.PlanDaySection
import com.easyeat.presentation.view.plan.PlanEditView
import io.paperdb.Paper
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe

@InjectViewState
class PlanEditPresenter : MvpPresenter<PlanEditView>() {

    private var plan: Plan = Plan()
    private lateinit var editedPlanDaySection: PlanDaySection

    init {
        EventBus.getDefault().register(this)
        viewState.onPlanLoaded()
        viewState.initToolbar()
    }

    fun startRecipeSelect(planDay: PlanDaySection) {
        editedPlanDaySection = planDay
        viewState.onStartRecipeSelect()
    }

    fun savePlan(title: String, description: String) {
        plan.title = title
        plan.description = description
        if (plan.title == "") {
            return
        }
        Paper.book(Books.Plan).write(plan.id, plan)
        EventBus.getDefault().post(PlanSaveAction())
    }

    @Subscribe
    fun onRecipeSelected(action: RecipeSelectedAction) {
        editedPlanDaySection.recipes.add(action.recipe)
        viewState.onRecipeSelected(editedPlanDaySection)
    }

    fun getDayByNumber(dayNumber: Int): PlanDay = plan.days[dayNumber]
}