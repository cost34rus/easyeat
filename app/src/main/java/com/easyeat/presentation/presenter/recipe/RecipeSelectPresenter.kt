package com.easyeat.presentation.presenter.recipe

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.easyeat.actions.RecipeSelectedAction
import com.easyeat.models.Books
import com.easyeat.models.Recipe
import com.easyeat.presentation.view.recipe.RecipeSelectView
import io.paperdb.Paper
import org.greenrobot.eventbus.EventBus

@InjectViewState
class RecipeSelectPresenter : MvpPresenter<RecipeSelectView>() {
    private var paper = Paper.book(Books.Recipe)

    init {
        val recipes = paper.allKeys.map { paper.read<Recipe>(it) }
        viewState.onRecipeLoad(recipes)
        viewState.initToolbar()
    }

    fun selectRecipe(recipe: Recipe) {
        EventBus.getDefault().post(RecipeSelectedAction(recipe))
        viewState.onRecipeSelected(recipe)
    }
}
