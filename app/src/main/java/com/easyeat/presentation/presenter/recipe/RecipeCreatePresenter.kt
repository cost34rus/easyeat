package com.easyeat.presentation.presenter.recipe

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.easyeat.actions.AddStepAction
import com.easyeat.actions.SetIngredientsAction
import com.easyeat.actions.SetRecipeInfoAction
import com.easyeat.models.Books
import com.easyeat.models.Recipe
import com.easyeat.presentation.view.recipe.RecipeCreateView
import io.paperdb.Paper
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe

@InjectViewState
class RecipeCreatePresenter : MvpPresenter<RecipeCreateView>() {
    private val recipe = Recipe("", "")

    init {
        EventBus.getDefault().register(this)
        viewState.initToolbar()
        viewState.initActions()
    }

    fun saveRecipe() {
        Paper.book(Books.Recipe).write(recipe.id, recipe)
    }

    @Subscribe
    fun onSetRecipeInfo(action: SetRecipeInfoAction) {
        recipe.title = action.title
        recipe.description = action.description
    }

    @Subscribe
    fun onSetIngredients(action: SetIngredientsAction) {
        recipe.ingredients = action.ingredients
    }

    @Subscribe
    fun onAddStep(action: AddStepAction) {
        recipe.steps.add(action.step)
    }
}
