package com.easyeat.presentation.presenter.plan

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.easyeat.actions.PlanSaveAction
import com.easyeat.actions.RecipeSelectedAction
import com.easyeat.models.Books
import com.easyeat.models.Plan
import com.easyeat.models.PlanDay
import com.easyeat.models.Recipe
import com.easyeat.presentation.view.plan.PlanView
import io.paperdb.Paper
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe

@InjectViewState
class PlanPresenter : MvpPresenter<PlanView>() {
    private var paper = Paper.book(Books.Plan)

    init {
        EventBus.getDefault().register(this)
        loadPlans()
        viewState.initToolbar()
        viewState.initMenuActions()
    }

    @Subscribe
    fun onPlanSave(action: PlanSaveAction) {
        loadPlans()
    }

    private fun loadPlans() {
        val plans = paper.allKeys.map { paper.read<Plan>(it) }
        viewState.onPlanLoad(plans)
    }
}
