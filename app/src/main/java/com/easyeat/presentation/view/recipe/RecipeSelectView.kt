package com.easyeat.presentation.view.recipe

import com.arellomobile.mvp.MvpView
import com.easyeat.models.Recipe
import com.easyeat.presentation.view.plan.base.ToolbarView

interface RecipeSelectView : MvpView, ToolbarView {
    fun onRecipeLoad(recipeList: List<Recipe>)
    fun onRecipeSelected(recipe: Recipe)
}
