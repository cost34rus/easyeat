package com.easyeat.presentation.view.plan.base

interface ToolbarView {
    fun initToolbar()
}