package com.easyeat.presentation.view.plan

import com.arellomobile.mvp.MvpView
import com.easyeat.models.Plan
import com.easyeat.presentation.view.plan.base.ToolbarView

interface PlanView : MvpView, ToolbarView {
    fun onPlanLoad(plans: List<Plan>)
    fun initMenuActions()
}
