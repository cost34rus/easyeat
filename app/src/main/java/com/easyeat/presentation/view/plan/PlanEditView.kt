package com.easyeat.presentation.view.plan

import com.arellomobile.mvp.MvpView
import com.easyeat.models.Plan
import com.easyeat.models.PlanDay
import com.easyeat.models.PlanDaySection
import com.easyeat.models.Recipe
import com.easyeat.presentation.view.plan.base.ToolbarView

interface PlanEditView : MvpView, ToolbarView {
    fun onPlanLoaded()
    fun onStartRecipeSelect()
    fun onRecipeSelected(planDaySection: PlanDaySection)
}
