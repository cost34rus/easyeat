package com.easyeat.presentation.view.recipe

import com.arellomobile.mvp.MvpView
import com.easyeat.presentation.view.plan.base.ToolbarView

interface RecipeCreateView : MvpView, ToolbarView {
    fun recipeLoad()
    fun initActions()
}
